<?php require_once('inc/head.php'); ?>
   <body>
<?php require_once('inc/header.php'); ?>
<?php require_once('inc/page-heading.php'); ?>
<!--TODO: change name of class-->
<div class="bg-container">
  <img src="images/contacts-bg-1.png" alt="">  
<!--</div>-->
  <div class="container" >
<?php require_once('inc/breadcrumbs.php'); ?>
  </div>
  <!--NEWS PAGE-->
  <section class="page-content container">
    <div class="page-content-heading">продукция</div>
    <div id="production-list" class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 production-list-item img-title-elem">
            <a href="production_sub.php">
                <div class="img-container">
                    <img src="images/prod-1.png" alt="some alt" class="">
                    <!--<img src="images/prod-2.png" alt="" class="img-default">-->
                </div>
                <div class="title-container">
                    <h1>10 000 кВт</h1>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 production-list-item img-title-elem">
            <a href="production_sub.php">
                <div class="img-container">
                    <img src="images/prod-1.png" alt="some alt" class="">
                    <!--<img src="images/prod-2.png" alt="" class="img-default">-->
                </div>
                <div class="title-container">
                    <h1>10 000 кВт</h1>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 production-list-item img-title-elem">
            <a href="production_sub.php">
                <div class="img-container">
                    <img src="images/prod-1.png" alt="some alt" class="">
                    <!--<img src="images/prod-2.png" alt="" class="img-default">-->
                </div>
                <div class="title-container">
                    <h1>10 000 кВт</h1>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 production-list-item img-title-elem">
            <a href="production_sub.php">
                <div class="img-container">
                    <img src="images/prod-1.png" alt="some alt" class="">
                    <!--<img src="images/prod-2.png" alt="" class="img-default">-->
                </div>
                <div class="title-container">
                    <h1>10 000 кВт</h1>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 production-list-item img-title-elem">
            <a href="production_sub.php">
                <div class="img-container">
                    <img src="images/prod-1.png" alt="some alt" class="">
                    <!--<img src="images/prod-2.png" alt="" class="img-default">-->
                </div>
                <div class="title-container">
                    <h1>10 000 кВт</h1>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 production-list-item img-title-elem">
            <a href="production_sub.php">
                <div class="img-container">
                    <img src="images/prod-1.png" alt="some alt" class="">
                    <!--<img src="images/prod-2.png" alt="" class="img-default">-->
                </div>
                <div class="title-container">
                    <h1>10 000 кВт</h1>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 production-list-item img-title-elem">
            <a href="production_sub.php">
                <div class="img-container">
                    <img src="images/prod-1.png" alt="some alt" class="">
                    <!--<img src="images/prod-2.png" alt="" class="img-default">-->
                </div>
                <div class="title-container">
                    <h1>ДЕКОРАТИВНЫЕ ПОКРЫТИЯ</h1>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 production-list-item img-title-elem">
            <a href="production_sub.php">
                <div class="img-container">
                    <img src="images/prod-1.png" alt="some alt" class="">
                    <!--<img src="images/prod-2.png" alt="" class="img-default">-->
                </div>
                <div class="title-container">
                    <h1>СОБСТВЕННЫЕ ПРОЕКТЫ</h1>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 production-list-item img-title-elem">
            <a href="production_sub.php">
                <div class="img-container">
                    <img src="images/prod-1.png" alt="some alt" class="">
                    <!--<img src="images/prod-2.png" alt="" class="img-default">-->
                </div>
                <div class="title-container">
                    <h1>10 000 кВт</h1>
                </div>
            </a>
        </div>

    </div>
  </section>
  <!--how to change bg image in bitrix admin?-->
    <section class="bg-s-gons">
        <div class="container">
            <article class="production-page-article">
                <p>В конце 2005 года было начато строительство ВЛ 110 кВ «Мантурово-Кроностар» в энергосистеме 
                    Костромаэнерго с применением многогранных опор, производства <strong>ОАО «Опытный завод Гидромонтаж»</strong>. 
                    С этого момента началась российская история массового применения стальных многогранных опор.
                </p>
                <p>Данная тематика прошла тернистый путь от разовых попыток единичного, пилотного применения до 
                    государственной целевой программы и включения в техническую политику электросетевых компаний 
                    в качестве одной из базовых технологий.
                </p>
                <p>Благодаря дальновидному подходу ОАО «ФСК ЕЭС» и ОАО «Холдинг МРСК», усилиям ведущих проектных 
                    институтов и заводов-изготовителей было сокращено 50-летнее технологическое отставание России 
                    от мировой практики электросетевого строительства.
                </p>
                <p>С уверенностью можно сказать, что на данный момент российские производители и проектировщики 
                    владеют технологией разработки и производства СМО ничуть не хуже, а возможно 
                    и лучше мировых лидеров данной отрасли.
                </p>
                <p>В освещении тематики стальных многогранных опор немало внимания уделяется вопросам оптимизации 
                    конструкций опор и индивидуального проектирования ВЛ с применением СМО.
                </p>
            </article>
        </div>
    </section>
<!--background div end-->
</div>

<!--NEWS PAGE-->
<?php require_once('inc/footer.php'); ?>
   </body>
</html>