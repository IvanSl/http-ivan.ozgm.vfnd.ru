<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Демонстрационная версия продукта «1С-Битрикс: Управление сайтом»");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("ozgm");
?><div id="about">
    <div class="bg-about">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h1 class="text-white">АО Опытный завод ‹‹Гидромонтаж››</h1>
                <p class="text-white">
                    Предприятие промышленного холдинга
                </p>
                <div class="bars-cont hidden-xs">
                    <img alt="bars" src="/bitrix/templates/ozgm/images/bars.png" class="bars">
                </div>
            </div>
            <div class="col-xs-12 col-sm-6">
                <p class="h1">
                    Производство опор вл и лэп
                </p>
                <div class="about-icons">
                    <figure class="icons-1 icons"> <img alt="somealt" src="/bitrix/templates/ozgm/images/diagramm_icon.png">
                        <p>
                            Мощности: <br class="visible-sm">
                            40 000 т. в год
                        </p>
                    </figure> <figure class="icons-2 icons"> <img src="/bitrix/templates/ozgm/images/factory_icon.png" alt="">
                        <p>
                            Собственное производство
                        </p>
                    </figure> <figure class="icons-3 icons"> <img src="/bitrix/templates/ozgm/images/lap_icon.png" alt="">
                        <p>
                            Эстетика
                        </p>
                    </figure> <figure class="icons-4 icons"> <img src="/bitrix/templates/ozgm/images/three_arrows_icon.png" alt="">
                        <p>
                            Адаптивность
                        </p>
                    </figure> <figure class="icons-5 icons"> <img src="/bitrix/templates/ozgm/images/up_arrow_icon.png" alt="">
                        <p>
                            Экономическая эффективность
                        </p>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</div>
    <!--video-container-->
    <div class="bg-gray">
        <div id="activities" class="container">
            <div class="row">
                <div class="activity img-title-elem col-xs-12 col-sm-6 col-md-3">
                    <a href="">
                        <div class="img-container">
                            <img src="/bitrix/templates/ozgm/images/act2.png" alt="">
                            <!--<img class="img-default" src="images/act1.png" alt="">-->
                        </div>
                        <div class="title-container">
                            <h1>производство типовых опор</h1>
                        </div>
                    </a>
                </div>
                <div class="activity img-title-elem col-xs-12 col-sm-6 col-md-3">
                    <a href="">
                        <div class="img-container">
                            <img src="/bitrix/templates/ozgm/images/act2.png" alt="">
                            <!--<img class="img-default" src="images/act2.png" alt="">-->
                        </div>
                        <div class="title-container">
                            <h1>Производство нетиповых конструкций</h1>
                        </div>
                    </a>
                </div>
                <!--add after 2nd div to fix bug with different height of images--> <!--через админку лучше заливат изображения одного размера-->
                <div class="activity img-title-elem col-xs-12 col-sm-6 col-md-3">
                    <a href="">
                        <div class="img-container">
                            <img src="/bitrix/templates/ozgm/images/act2.png" alt="">
                            <!--<img class="img-default" src="images/act3.png" alt="">-->
                        </div>
                        <div class="title-container">
                            <h1>Монтаж стальных многогранных опор</h1>
                        </div>
                    </a>
                </div>
                <div class="activity img-title-elem col-xs-12 col-sm-6 col-md-3">
                    <a href="">
                        <div class="img-container">
                            <img src="/bitrix/templates/ozgm/images/act2.png" alt="">
                            <!--<img class="img-default" src="images/act4.png" alt="">-->
                        </div>
                        <div class="title-container">
                            <h1>Проектирование высоковольтных линий</h1>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "ozgm_news_list_index",
    array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "ADD_SECTIONS_CHAIN" => "Y",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "FIELD_CODE" => array(
            0 => "",
            1 => "",
        ),
        "FILTER_NAME" => "",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "IBLOCK_ID" => "3",
        "IBLOCK_TYPE" => "news",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
        "INCLUDE_SUBSECTIONS" => "Y",
        "MESSAGE_404" => "",
        "NEWS_COUNT" => "3",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Новости",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "PROPERTY_CODE" => array(
            0 => "",
            1 => "",
        ),
        "SET_BROWSER_TITLE" => "Y",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "Y",
        "SHOW_404" => "N",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_BY2" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_ORDER2" => "ASC",
        "COMPONENT_TEMPLATE" => "ozgm_news_list_index"
    ),
    false
);?>

    <div class="bg-s-gons">
        <div class="container">
            <div class="row" id="partners">
                <h1>наши партнеры</h1>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <img src="/bitrix/templates/ozgm/images/partner-1.png" alt="">
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <img src="/bitrix/templates/ozgm/images/partner-1.png" alt="">
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <img src="/bitrix/templates/ozgm/images/partner-1.png" alt="">
                </div>
                <div class="col-xs-6 col-sm-6 col-md-3">
                    <img src="/bitrix/templates/ozgm/images/partner-1.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>