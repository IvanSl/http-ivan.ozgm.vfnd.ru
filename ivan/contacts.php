<?php require_once('inc/head.php'); ?>
   <body>
<?php require_once('inc/header.php'); ?>
<?php require_once('inc/page-heading.php'); ?>
<!--TODO: change name of class-->
<div class="bg-container">
  <img src="images/contacts-bg-1.png" alt="">  
<!--</div>-->
  <div class="container" >
<?php require_once('inc/breadcrumbs.php'); ?>
  </div>
  <!--for main(unique) elements of page-->
  <section class="page-content container">
    <div class="page-content-heading">Контакты</div>
    <form id="contacts-page-form" class="ozgm-form row">
      <h2>Для связи с нами вы можете воспользоваться контактной формой.</h2>
      <div class="col-xs-12 col-md-6">
        <div class="form-group">
          <!--<div class="col-xs-12 col-sm-4">          -->
            <label for="contacts-page-form-name">Контактное лицо&nbsp;<span>*</span></label>
          <!--</div>-->
          <!--<div class="col-xs-12 col-sm-8"> -->
            <input type="text" class="form-control" id="contacts-page-form-name" placeholder="" required>
          <!--</div>-->
        </div>
        <div class="form-group">
          <label for="contacts-page-form-tel">Телефон&nbsp;<span>*</span></label>
          <input type="tel" class="form-control" id="contacts-page-form-tel" placeholder="" required>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 ozgm-form-md-right">
        <div class="form-group">
          <label for="contacts-page-form-email">E-mail&nbsp;<span>*</span></label>
          <input type="email" class="form-control" id="contacts-page-form-email" placeholder="" required>
        </div>
        <div class="form-group">
          <label for="contacts-page-form-textarea">Вопрос&nbsp;<span>*</span></label>
          <textarea class="form-control" id="contacts-page-form-textarea" required></textarea>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 submit-btn-container">
        <p><span>*</span>&nbsp;-&nbsp;Все поля обязательны для заполнения</p>
        <button type="submit" class="btn">Отправить</button>
      </div>
    </form>
  </section>
<!--background div end-->
</div>
<div id="map-container">
  <div class="container">
    <div class="row">
      <div id="map">
        <div id="map-info">
              <p><a href="">143345, Московская область,<br>Наро-фоминский район, пос. Селятино,<br>территроия завода &lsaquo;&lsaquo;Гидромонтаж&rsaquo;&rsaquo;</a></p>
              <a href="tel:+74957204964">Телефон: +7 (495) 720-49-64</a><br>
              <a href="fax:+7.495.720.4972">Тел./Факс: +7 (495) 720-49-72</a><br>
              <a href="mailto:info@ozgm.ru">Электронная почта: info@ozgm.ru</a>
              <p class="work-time">Время работы:&nbsp; <span>пн-пт 10-18</span> </p>
        </div>  
      </div>
    </div>
  </div>
</div>
<!--CONTACTS PAGE-->
<?php require_once('inc/footer.php'); ?>
      <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
      <script src="js/ymap.min.js"></script>
   </body>
</html>