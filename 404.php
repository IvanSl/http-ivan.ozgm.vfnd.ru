<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");
?>
<section class="row container-white">
<?php
$APPLICATION->IncludeComponent("bitrix:main.map", "ozgm_site_map", Array(
	"LEVEL" => "3",	// Максимальный уровень вложенности (0 - без вложенности)
		"COL_NUM" => "2",	// Количество колонок
		"SHOW_DESCRIPTION" => "Y",	// Показывать описания
		"SET_TITLE" => "N",	// Устанавливать заголовок страницы
		"CACHE_TIME" => "3600",	// Время кеширования (сек.)
		"COMPONENT_TEMPLATE" => ".default",
		"CACHE_TYPE" => "A",	// Тип кеширования
	),
	false
);
?>
</section>
    <!--background div end-->
    </div>
<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>