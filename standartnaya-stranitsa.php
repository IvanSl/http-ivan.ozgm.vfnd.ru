<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("стандартная страница");
?>
  <?php  $APPLICATION->AddChainItem($APPLICATION->GetTitle()); ?>
        <article class="ozgm-article row container-white">
	<p>
		 В промышленный холдинг «Гидромонтаж» так же входят:

	<ul>
		<li>
		<p>
 <strong>ЗАО «Гидростальконструкция»</strong> – компания по производству гидромеханического оборудования, строительных металлконструкций, многогранных опор ЛЭП, резервуаров различного назначения и крановых механизмов.
		</p>
 </li>
		<li>
		<p>
 <strong>ЗАО «Гофросталь»</strong> – единственный в СНГ производитель металлических гофрированных конструкций всех типов и сечений.
		</p>
 </li>
		<li>
		<p>
 <strong>ООО «СевЗапРегионСтрой»</strong> – компания, специализирующаяся на проектировании и строительстве объектов с масштабным применением продукции холдинга, обладающая всеми необходимым допусками и разрешениями.
		</p>
 </li>
		<li>
		<p>
 <strong>ООО ТК «Гидромонтаж»</strong> – транспортная компания, оказывающая услуги по перевозке грузов любым видом транспорта.
		</p>
 </li>
	</ul>
	<p>
		 Нашей компанией были спроектрированы и осуществлены несколько десятков проектов.
	</p>
	<p>
		 Самыми последними стали объекты в Сочи к Олимпиаде-2014:
	</p>
	<ul>
		<li>
		<p>
			 Инженерная защита территории, кабельных и воздушных линий (110кВ) в районе плато Роза Хутор (селезащитная галерея) в Краснодарском крае, Красная поляна.
		</p>
 </li>
		<li>
		<p>
			 Проект совмещенной автомобильной и железной дороги «Адлер - Альпика Сервис».
		</p>
 </li>
		<li>
		<p>
			 Производство и поставка олимпийской символики «Сочи 2014» в виде больших олимпийских колец, расположенных на крупных магистралях Сочи.
		</p>
 </li>
	</ul>
	<p>
		 С остальными объектами вы можете ознакомиться в разделе сайта: «Галерея Объектов» и в разделе «Новости».
	</p>
	<p>
 <strong>Приглашаем вас к взаимовыгодному сотрудничеству!</strong>
	</p>
 </article>
	<!--</div>--> </section>
    <?$APPLICATION->IncludeFile(
        $APPLICATION->GetTemplatePath("include_areas/certeficates_inc.php"),
        Array(),
        Array("MODE"=>"php")
    );?>
	<!--background div end-->
</div>
    <script
            src="https://code.jquery.com/jquery-1.12.4.js"
            integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
            crossorigin="anonymous"></script>

    <script>
        $(document).ready(function () {
            $('#certificates .certificates-item:lt(6)').show();
            $('.less').hide();
            var items =  12;
            var shown =  6;
            $('.more-certeficates').click(function () {
//                $('.less').show();
                shown = $('.certificates-item:visible').size()+6;
                if(shown< items) {$('.certificates-item:lt('+shown+')').show(300);}
                else {$('.certificates-item:lt('+items+')').show(300);
                    $('.more').hide();
                }
            });
            $('.less').click(function () {
                $('.gallery li').not(':lt(3)').hide(300);
                $('.more').show();
                $('.less').hide();
            });
        });
    </script>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>