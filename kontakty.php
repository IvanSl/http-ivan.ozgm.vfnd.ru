<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>
<?=$APPLICATION->AddChainItem($APPLICATION->GetTitle());?>

        <?$APPLICATION->IncludeFile(
            $APPLICATION->GetTemplatePath("include_areas/contacts_page_form_inc.php"),
            Array(),
            Array("MODE"=>"php")
        );?>
    </section>
    <!--background div end-->
    </div>
    <div id="map-container">
        <div class="container">
            <div class="row">
                <div id="map">
                    <div id="map-info">
                        <p><a href="">143345, Московская область,<br>Наро-фоминский район, пос. Селятино,<br>территроия завода &lsaquo;&lsaquo;Гидромонтаж&rsaquo;&rsaquo;</a></p>
                        <a href="tel:+74957204964">Телефон: +7 (495) 720-49-64</a><br>
                        <a href="fax:+7.495.720.4972">Тел./Факс: +7 (495) 720-49-72</a><br>
                        <a href="mailto:info@ozgm.ru">Электронная почта: info@ozgm.ru</a>
                        <p class="work-time">Время работы:&nbsp; <span>пн-пт 10-18</span> </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/ymap.min.js"></script>
<!--    <script src="--><?//=SITE_TEMPLATE_PATH?><!--/js/contacts_form.js"></script>-->


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>