<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>



<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<?$APPLICATION->ShowHead()?>
    <?php

    CUtil::InitJSCore();
    CJSCore::Init(array("jquery2", "bootstrap"))

    ?>
<title><?$APPLICATION->ShowTitle()?></title>
</head>

<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<body>
<?php

if(CSite::InDir('/index.php')) { ?>
    <div id="video-container">
    <!--<div class="fix-container">-->

    <?php if (!$APPLICATION->GetShowIncludeAreas()) { ?>

        <video poster="" autoplay loop id="main-page-video">



        <!--<source src="video/duel.ogv" type='video/ogg; codecs="theora, vorbis"'>-->
        <!--<source src="video/duel.webm" type='video/webm; codecs="vp8, vorbis"'>-->
        <!--<source src="video/shutterstock_v12021443.mov">-->
        <source src="<?=SITE_TEMPLATE_PATH?>/video/shutterstock_v12021443_CLIPCHAMP_keep.mp4" type="video/mp4">
        <!--Тег video не поддерживается вашим браузером. -->
        <!--<a href="video/duel.mp4">Скачайте видео</a>.-->
    </video>
    <!--</div>-->
    <? } ?>
<?php }  ?>

<!--HEADER-->
<header id="header">
    <!--<img src="" alt="header-bg" id="header-bg">-->
    <div class="bg-dark-blue">
        <div class="container">
            <div class="row h-top">
                <a href="/">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="" class="logo">
                </a>

                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "header_nav",
                    Array(
                        "ROOT_MENU_TYPE" => "top",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "Y",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => Array()
                    )
                );?>
                <a href="" id="header-btn">презентация <span class="hidden-xs hidden-md">компании</span>&nbsp;<span class="header-btn-arrow">&rsaquo;&rsaquo;</span></a>
            </div>
        </div>
    </div>
</header>
<!--HEADER-->
<!--to page template-->
<?php

if(CSite::InDir('/index.php')) {


} else { ?>

    <!--PAGE HEADING-->
    <div class="bg-gray-whisper">
        <div class="page-heading container">
            <p class="h1">АО &lsaquo;&lsaquo;Опытный завод &lsaquo;&lsaquo;Гидромонтаж&rsaquo;&rsaquo;</p>
            <p class="page-heading-sub-title-1">Предприятие промышленного холдинга</p>
            <p class="page-heading-sub-title-2">Производство опор вл и лэп</p>
        </div>
    </div>
    <!--PAGE HEADING-->
        <div class="bg-container">
            <img src="/bitrix/templates/ozgm/images/contacts-bg-1.png" alt="">
            <!--</div>-->
            <?php
            $curr_uri = $APPLICATION->GetCurUri();
            if (strpos($curr_uri,'ozgm_products/') == false) { ?>
            <div class="container">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:breadcrumb",
                    "breadcrumbs",
                    Array(
                        "PATH" => "",
                        "SITE_ID" => "",
                        "START_FROM" => "0"
                    )
                );?>
            </div>
            <?php } ?>

            <section class="page-content container">
                <?php             if (strpos($curr_uri,'ozgm_products/') == true) { ?>
                    <div class="page-content-heading prodution-section-page-heading">
                        <h1>
                            <?$APPLICATION->ShowTitle(false)?>
                        </h1>

                    </div>
                    <?php } else  { ?>

                    <div class="page-content-heading">
                        <h1>
                            <?$APPLICATION->ShowTitle(false)?>
                        </h1>

                    </div>
                <? } ?>
<? } ?>

