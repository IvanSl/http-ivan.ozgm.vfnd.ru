<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>

<!--FOOTER-->
<div class="bg-black">
    <div class="container" id="footer">
        <div class="row">
            <div class="col-xs-12">
                <img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="" class="logo">
                <h1>АО Опытный завод &lsaquo;&lsaquo;Гидромонтаж&rsaquo;&rsaquo; 1992-2017</h1>
                <div class="footer-address">
                    <p><a href="">143345, Московская область,<br>Наро-фоминский район, пос. Селятино,<br>территроия завода &lsaquo;&lsaquo;Гидромонтаж&rsaquo;&rsaquo;</a></p>
                </div>
                <div class="footer-contacts">
                    <a href="tel:+74957204964">Телефон: <span>+7 (495) 720-49-64</span></a><br>
                    <a href="fax:+7.495.720.4972">Тел./Факс: <span>+7 (495) 720-49-72</span></a><br>
                    <a href="mailto:info@ozgm.ru">Электронная почта: <span>info@ozgm.ru</span></a>
                </div>
            </div>
            <div class="col-xs-12 footer-nav">
                <?$APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "footer_nav",
                    Array(
                        "ROOT_MENU_TYPE" => "top",
                        "MAX_LEVEL" => "1",
                        "CHILD_MENU_TYPE" => "left",
                        "USE_EXT" => "Y",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "MENU_CACHE_GET_VARS" => Array()
                    )
                );?>
                <a href="http://vefound.com" class="vefound-info"><span class="hidden-md">Создание и поддержка сайта: </span>veFound.com</a>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="contacts_form_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Спасибо</h4>
            </div>
            <div class="modal-body">
                <h3>Ваша заявка принята</h3>
            </div>
        </div>
    </div>
</div>

<script src="<?=SITE_TEMPLATE_PATH?>/js/script.min.js"></script>
</body>
</html>