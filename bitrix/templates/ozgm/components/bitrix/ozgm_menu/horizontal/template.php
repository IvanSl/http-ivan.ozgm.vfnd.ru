<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<nav class="header-nav">

	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<div id="navbar" class="ozgm-nav navbar-collapse collapse in">
		<ul class="">
<? foreach($arResult as $arItem):?>
		<li><a href="<?=htmlspecialcharsbx($arItem["LINK"])?>"><?=htmlspecialcharsbx($arItem["TEXT"])?><span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
<?endforeach?>
		<!--<li><a href="about.php">О заводе<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
		<li><a href="production.php">Продукция<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
		<li><a href="services.php">Услуги<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
		<li><a href="news.php">Пресс-центр<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
		<li><a href="contacts.php">Контакты<span>&nbsp;&rsaquo;&rsaquo;</span></a></li>-->
		</ul>
	</div>
</nav>
<?endif?>