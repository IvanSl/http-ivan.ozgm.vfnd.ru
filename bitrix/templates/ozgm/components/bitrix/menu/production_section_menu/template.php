<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?php global $APPLICATION;

if (!function_exists("GetTreeRecursive")) {
    $aMenuLinks = $APPLICATION->IncludeComponent(
        "bitrix:menu.sections",
        "",
        array(
            "IBLOCK_TYPE" => "ozgm_products",
            "IBLOCK_ID" => "12",
            "SECTION_URL" => "#SITE_DIR#/info/news/#SECTION_CODE#/",
            "DEPTH_LEVEL" => "3",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "IS_SEF" => "N",
            "ID" => $_REQUEST["ID"]
        ),
        false
    );
}
?>