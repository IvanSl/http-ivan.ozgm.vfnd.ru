<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

    <nav class="ozgm-nav">
        <ul>
            <? foreach($arResult as $arItem):?>
                <li><a href="<?=htmlspecialcharsbx($arItem["LINK"])?>"><?=htmlspecialcharsbx($arItem["TEXT"])?><span>&nbsp;&rsaquo;&rsaquo;</span></a></li>
            <?endforeach?>
        </ul>
    </nav>
<?endif?>