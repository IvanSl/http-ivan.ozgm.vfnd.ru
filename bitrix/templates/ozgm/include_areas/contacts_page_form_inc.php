<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<form id="contacts-page-form" class="ozgm-form row container-white">
    <h2>Для связи с нами вы можете воспользоваться контактной формой.</h2>
    <div class="col-xs-12 col-md-6">
        <div class="form-group">

            <label for="contacts-page-form-name">Контактное лицо&nbsp;<span>*</span></label>

            <input type="text" class="form-control" id="contacts-page-form-name" placeholder="" required>

        </div>
        <div class="form-group">
            <label for="contacts-page-form-tel">Телефон&nbsp;<span>*</span></label>
            <input type="tel" class="form-control" id="contacts-page-form-tel" placeholder="" required>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 ozgm-form-md-right">
        <div class="form-group">
            <label for="contacts-page-form-email">E-mail&nbsp;<span>*</span></label>
            <input type="email" class="form-control" id="contacts-page-form-email" placeholder="" required>
        </div>
        <div class="form-group">
            <label for="contacts-page-form-textarea">Вопрос&nbsp;<span>*</span></label>
            <textarea class="form-control" id="contacts-page-form-textarea" required></textarea>
        </div>
    </div>
    <div class="col-xs-12 col-md-6 submit-btn-container">
        <p><span>*</span>&nbsp;-&nbsp;Все поля обязательны для заполнения</p>
        <button type="submit" class="btn">Отправить</button>
    </div>
</form>

    <div id="contacts_form_modal">
        <div class="contacts_form_modal">
            <p>Ваша заявка отправлена</p>
        </div>
    </div>


<!--    <script-->
<!--            src="https://code.jquery.com/jquery-1.12.4.js"-->
<!--            integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="-->
<!--            crossorigin="anonymous">-->
<!---->
<!--    </script>-->

<!--        <script src="bitrix/templates/ozgm/js/modal.js"></script>-->




    <script>
        $(document).ready(function(){

//            alert('modal');
        });
    </script>
    <script>

        $(document).ready(function(){
            // alert('ready');
            $('#contacts-page-form').submit(function (e) {
                e.preventDefault();
//                alert("go 1");
                go();
            })

            // $('#modal_btn').click(function(e){
// e.preventDefault();

// собираем данные с формы
//         if (($('#name').val().length>0)&&($('#email').val().length>0)&&($('#phone').val().length>0)) {
//             go();
//
//         } else {
//
//             $('.messages').html('Не все поля заполнены').addClass('error-red').show();
//         }
            //
            function go() {

                var name =$('#contacts-page-form-name').val();
                var email =$('#contacts-page-form-email').val();
                var phone =$('#contacts-page-form-tel').val();
                var form_text=$('#contacts-page-form-textarea').val();
//                alert(name);

                $.ajax({
                    url: "mail.php", // куда отправляем
                    type: "post", // метод передачи
                    dataType: "json", // тип передачи данных
                    data: { // что отправляем
                        "name": name,
                        "email": email,
                        "phone": phone,
                        "form_text": form_text
                    },
// после получения ответа сервера
                    success: function(data){
                        // $('.messages').html("<span style='color: green;'>Скоро с Вами свяжется наш менеджер</span>");
                        // $('.modal-title').text("Спасибо!");
                        // $("#modal_form").hide();
//                         alert(data);
//                        function dump(obj) {
//                            var out = '';
//                            for (var i in obj) {
//                                out += i + ": " + obj[i] + "\n";
//                            }
//
//                            alert(out);
//                            alert("test");
//                        }
//                        dump(data);
                        $('#contacts_form_modal_container').show();
                    }
                });
            }

// отправляем данные
//     });
//     $("a[data-toggle=modal]").on("click", function() {
//         $("#modal_form").show();
//         $('.messages').hide();
//     })
        });

    </script>

