<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div id="map-container">
    <div class="container">
        <div class="row">
            <div id="map">
                <div id="map-info">
                    <p><a href="">143345, Московская область,<br>Наро-фоминский район, пос. Селятино,<br>территроия завода &lsaquo;&lsaquo;Гидромонтаж&rsaquo;&rsaquo;</a></p>
                    <a href="tel:+74957204964">Телефон: +7 (495) 720-49-64</a><br>
                    <a href="fax:+7.495.720.4972">Тел./Факс: +7 (495) 720-49-72</a><br>
                    <a href="mailto:info@ozgm.ru">Электронная почта: info@ozgm.ru</a>
                    <p class="work-time">Время работы:&nbsp; <span>пн-пт 10-18</span> </p>
                </div>
            </div>
        </div>
    </div>
</div>