<?php
$msg_box = ""; // в этой переменной будем хранить сообщения формы
$errors = array(); // контейнер для ошибок
// проверяем корректность полей
if($_POST['email'] =="") $errors[] ="Поле 'Ваше e-mail' не заполнено!";
if($_POST['phone'] =="") $errors[] ="Поле 'Ваш телефон' не заполнено!";
if($_POST['name'] =="") $errors[] ="Поле 'Имя' не заполнено!";

// если форма без ошибок
if(empty($errors)){
// собираем данные из формы
    $message = "Имя пользователя: " . $_POST['name'] . "<br/>";
    $message .= "E-mail пользователя: " . $_POST['email'] . "<br/>";
    $message .= "Телефон: " . $_POST['phone'];
    send_mail($message); // отправим письмо
// выведем сообщение об успехе
    $msg_box = "";
}else{
// если были ошибки, то выводим их
    $msg_box = "";
    foreach($errors as $one_error){
//        $msg_box .= "<span style='color: red;'>$one_error</span><br/>";
    }
}

// делаем ответ на клиентскую часть в формате JSON
//echojson_encode(array(
//    'result'=> $msg_box
//));


// функция отправки письма
function send_mail($message){
// почта, на которую придет письмо
$mail_to = "slojenikin2012@gmail.com";
// тема письма
$subject = "Письмо с обратной связи";

// заголовок письма
$headers= "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=utf-8\r\n"; // кодировка письма
$headers .= "From: <no-reply@test.com>\r\n"; // от кого письмо

// отправляем письмо
mail($mail_to, $subject, $message, $headers);
}

?>