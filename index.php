<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Демонстрационная версия продукта «1С-Битрикс: Управление сайтом»");
$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("ozgm");
?><div id="about">
	<div class="bg-about">
	</div>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<h1 class="text-white">АО Опытный завод ‹‹Гидромонтаж››</h1>
				<p class="text-white">
					 Предприятие промышленного холдинга
				</p>
				<div class="bars-cont hidden-xs">
 <img alt="bars" src="/bitrix/templates/ozgm/images/bars.png" class="bars">
				</div>
			</div>
			<div class="col-xs-12 col-sm-6">
				<p class="h1">
					 Производство опор вл и лэп
				</p>
				<div class="about-icons">
 <figure class="icons-1 icons"> <img alt="somealt" src="/bitrix/templates/ozgm/images/diagramm_icon.png">
					<p>
						 Мощности: <br class="visible-sm">
						 40 000 т. в год
					</p>
 </figure> <figure class="icons-2 icons"> <img src="/bitrix/templates/ozgm/images/factory_icon.png" alt="">
					<p>
						 Собственное производство
					</p>
 </figure> <figure class="icons-3 icons"> <img src="/bitrix/templates/ozgm/images/lap_icon.png" alt="">
					<p>
						 Эстетика
					</p>
 </figure> <figure class="icons-4 icons"> <img src="/bitrix/templates/ozgm/images/three_arrows_icon.png" alt="">
					<p>
						 Адаптивность
					</p>
 </figure> <figure class="icons-5 icons"> <img src="/bitrix/templates/ozgm/images/up_arrow_icon.png" alt="">
					<p>
						 Экономическая эффективность
					</p>
 </figure>
				</div>
			</div>
		</div>
	</div>
</div>
 <!--video-container-->
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"section_list_index_page", 
	array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "section_list_index_page",
		"COUNT_ELEMENTS" => "N",
		"IBLOCK_ID" => "12",
		"IBLOCK_TYPE" => "ozgm_products",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_URL" => "#SITE_DIR#/ozgm_products/#ID#",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SHOW_PARENT_NAME" => "N",
		"TOP_DEPTH" => "2",
		"VIEW_MODE" => "LINE"
	),
	false
);?> <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"ozgm_news_list_index",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "ozgm_news_list_index",
		"DETAIL_URL" => "/press/#ID#/",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "3",
		"IBLOCK_TYPE" => "news",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "3",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	)
);?>
 <?$APPLICATION->IncludeComponent(
	"bitrix:furniture.catalog.index",
	"partners_list_index",
	Array(
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000",
		"CACHE_TYPE" => "A",
		"IBLOCK_BINDING" => "element",
		"IBLOCK_ID" => "13",
		"IBLOCK_TYPE" => "news"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>